import scipy.io
import os
import numpy as np
from numpy.fft import fft, fftfreq, ifft
import matplotlib.pyplot as plt
from math import pi
import datetime
import matplotlib as mpl
from scipy.signal import butter, lfilter

def sharp_filter(data, lowcut, highcut, Fs):
	data_length = len(data) # number of samples
	freqs = fftfreq(data_length)*Fs # in Hz
	#freqs = (Fs/data_length)*np.arange(1,int(data_length/2)) # calculate manually
	freq_range = np.zeros_like(freqs, dtype=bool)
	for counter, i in enumerate(freqs):
		if i >= lowcut and i <= highcut:
			freq_range[counter] = 1
	spectrum_raw = fft(data)
	spectrum_filtered = spectrum_raw*freq_range
	return ifft(spectrum_filtered)

def butter_bandpass(lowcut, highcut, fs, order=5):
	nyq = 0.5 * fs
	low = lowcut / nyq
	high = highcut / nyq
	b, a = butter(order, [low, high], btype='band')
	return b, a

def butter_bandpass_filter(data, lowcut, highcut, fs, order=5):
	b, a = butter_bandpass(lowcut, highcut, fs, order=order)
	y = lfilter(b, a, data)
	return y

def LogTicks(a, b):
	ticks = []
	for i in range(int(np.floor(np.log10(a))), int(np.floor(np.log10(b)+1))):
		for j in range(1,10):
			if (10**i)*j >= a and (10**i)*j <=b:
				ticks = ticks + [(10**i)*j]
	return np.log10(ticks)
###############################################################################
###############################################################################
# Initial setup
for Flight in['inbound', 'outbound']:

	if Flight == 'inbound':
		# Start of the flight: 16:53:54.000
		start_time = 16*3600 + 53*60 + 54 # in seconds
		
		SI = np.zeros((1, 3)) # SI[i,0]: start hour of i-th signature
											# SI[i,1]: start minute of i-th signature
											# SI[i,2]: time interval of i-th signature in minute
	
		SI[0, 0] = 17
		SI[0, 1] = 0
		SI[0, 2] = 360
		
	elif Flight == 'outbound':
		# Start of the flight: 08:14:00.000
		start_time = 8*3600 + 14*60 + 0 # in seconds
	
		SI = np.zeros((1, 3)) # SI[i,0]: start hour of i-th signature
											# SI[i,1]: start minute of i-th signature
											# SI[i,2]: time interval of i-th signature in minute
	
		SI[0, 0] = 8
		SI[0, 1] = 20
		SI[0, 2] = 400
	###############################################################################
	###############################################################################
		# Load data
	Input_Directory = 'D:\\Jobs\\Stratodynamics\\MyWork\\Wake Vortices\\Data\\'
	Output_Directory = 'D:\\Jobs\\Stratodynamics\\MyWork\\Wake Vortices\\FilteredSignal\\' + Flight + '\\'
	
	os.chdir(Input_Directory)
	time = np.load(Flight + '_follower_GMTs.npy')[:, 0]
	signal_front = np.load(Flight + '_follower_front.npy')[:, 0]
	signal_aft = np.load(Flight + '_follower_aft.npy')[:, 0]
	###############################################################################
	###############################################################################
	# filtering
	Fs = 256 # Frequency of sampling, given in the readme file
	
	# time variable in this code is considered from the start of the flight
	time = time - time[0]
	
	# Infrasonic signals (measured in volts) are converted to milli-volts
	signal_front = signal_front * 1000
	signal_aft = signal_aft * 1000
	
	# Sensitivity of the microphones
	# The sensitivity of 1.5in microphone (installed in the front of the cabin) is 118mV/Pa
	Sensitivity_front = 118
	# The sensitivity of 3in microphone (installed in the aft of the cabin) is 338mV/Pa
	Sensitivity_aft = 338
	
	# Corresponding pressure perturbation in Pa
	signal_front = signal_front / Sensitivity_front
	signal_aft = signal_aft / Sensitivity_aft
	
	# Sensitivity of the microphones to low frequencies
	# The sensitivity of 1.5in microphone (installed in the front of the cabin) is 0.1 Hz
	lowcut_front = 0.1
	# The sensitivity of 3in microphone (installed in the aft of the cabin) is 0.001 Hz
	lowcut_aft = 0.001
	
	# Sharp filtering to keep infrasonic frequencies
	highcut = 20
	signal_front_sharpfilter = sharp_filter(signal_front, lowcut_front, highcut, Fs)
	signal_aft_sharpfilter = sharp_filter(signal_aft, lowcut_aft, highcut, Fs)
	
	# Smooth filtering to keep infrasonic frequencies 
	signal_front_butterfilter = butter_bandpass_filter(signal_front, 1, highcut, Fs, order=5)
	signal_aft_butterfilter = butter_bandpass_filter(signal_aft, 1, highcut, Fs, order=5)
	###############################################################################
	###############################################################################
	# calculation of spectra
	n = time.shape[0] # number of samples
	freqs = fftfreq(n)*Fs # in Hz, Fs(n/ws) is the sampling frequency in Hz
	mask = freqs > 0
	#dt = 1./Fs
	#freqs_manual = (1./(dt*n))*np.arange(1,int(n/2))
	
	ps_front = 2.0*(np.abs(fft(signal_front/(20e-6))/n)**2.0)[mask]
	ps_aft = 2.0*(np.abs(fft(signal_aft/(20e-6))/n)**2.0)[mask]
	
	ps_front_sharpfilter = 2.0*(np.abs(fft(signal_front_sharpfilter/(20e-6))/n)**2.0)[mask]
	ps_aft_sharpfilter = 2.0*(np.abs(fft(signal_aft_sharpfilter/(20e-6))/n)**2.0)[mask]
	
	ps_front_butterfilter = 2.0*(np.abs(fft(signal_front_butterfilter/(20e-6))/n)**2.0)[mask]
	ps_aft_butterfilter = 2.0*(np.abs(fft(signal_aft_butterfilter/(20e-6))/n)**2.0)[mask]
	###############################################################################
	###############################################################################
	# plot diagrams
	# front_Pressure
	plt.figure(Flight + '_front_Pressure')
	plt.plot(time, signal_front, 'b', label='raw')
	plt.plot(time, signal_front_sharpfilter, 'r', label='sharp filter')
	plt.plot(time, signal_front_butterfilter, 'g', label='butter filter')
	plt.axhline(y=np.mean(signal_front), color='k', linestyle='-', label='raw')
	plt.axhline(y=np.mean(signal_front_sharpfilter), color='k', linestyle='--', label='sharp filter')
	plt.axhline(y=np.mean(signal_front_butterfilter), color='k', linestyle=':', label='butter filter')
	plt.legend()
	plt.xlabel("Time (UTC)")
	plt.ylabel("Pressure (Pa)")
	plt.title(Flight + '_front_Pressure')
	plt.xlim([time[0], time[-1]])
	ax = plt.axes()
	xticks = [time[0]] + [SI[0, 0]*3600 + SI[0, 1]*60 - start_time + m*60 for m in range(0,int(SI[0, 2])+1,10)] + [int(time[-1])]
	labels = [str(datetime.timedelta(seconds=(start_time + second)))[:-3] for second in xticks] # [:-3] for removing seconds
	plt.xticks(xticks, labels)
	plt.xticks(rotation=-90)
	plt.gcf().set_size_inches((11.5, 8.5), forward=False)
	os.chdir(Output_Directory)
	plt.savefig(Flight + '_front_Pressure.tiff', bbox_inches="tight", pad_inches=0.06, dpi=200)
	plt.show()
	
	# aft_Pressure
	plt.figure(Flight + '_aft_Pressure')
	plt.plot(time, signal_aft, 'b', label='raw')
	plt.plot(time, signal_aft_sharpfilter, 'r', label='sharp filter')
	plt.plot(time, signal_aft_butterfilter, 'g', label='butter filter')
	plt.axhline(y=np.mean(signal_aft), color='k', linestyle='-', label='raw')
	plt.axhline(y=np.mean(signal_aft_sharpfilter), color='k', linestyle='--', label='sharp filter')
	plt.axhline(y=np.mean(signal_aft_butterfilter), color='k', linestyle=':', label='butter filter')
	plt.legend()
	plt.xlabel("Time (UTC)")
	plt.ylabel("Pressure (Pa)")
	plt.title(Flight + '_aft_Pressure')
	plt.xlim([time[0], time[-1]])
	ax = plt.axes()
	xticks = [time[0]] + [SI[0, 0]*3600 + SI[0, 1]*60 - start_time + m*60 for m in range(0,int(SI[0, 2])+1,10)] + [int(time[-1])]
	labels = [str(datetime.timedelta(seconds=(start_time + second)))[:-3] for second in xticks] # [:-3] for removing seconds
	plt.xticks(xticks, labels)
	plt.xticks(rotation=-90)
	plt.gcf().set_size_inches((11.5, 8.5), forward=False)
	os.chdir(Output_Directory)
	plt.savefig(Flight + '_aft_Pressure.tiff', bbox_inches="tight", pad_inches=0.06, dpi=200)
	plt.show()
	
	# front_spectra
	plt.figure(Flight + '_front_power spectra')
	plt.plot(np.log10(freqs[mask]), ps_front, 'b', label='raw')
	plt.plot(np.log10(freqs[mask]), ps_front_sharpfilter, 'r', label='sharp filter')
	plt.plot(np.log10(freqs[mask]), ps_front_butterfilter, 'g', label='butter filter')
	plt.legend()
	plt.xlabel("Frequency (Hz)")
	plt.ylabel("Power spectra (dB)")
	plt.title(Flight + '_front_power spectra')
	ax = plt.axes()
	ax.set_xticks(LogTicks(freqs[mask][0], freqs[mask][-1]), minor=True)
	xticks = [-4, -3, -2, -1., 0., 1., 2.]
	labels = ['$10^{%1.f}$' %x for x in xticks]
	plt.xticks(xticks, labels)
	plt.gcf().set_size_inches((11.5, 8.5), forward=False)
	os.chdir(Output_Directory)
	plt.savefig(Flight + '_front_power spectra.tiff', bbox_inches="tight", pad_inches=0.06, dpi=200)
	plt.show()
	
	# aft_spectra
	plt.figure(Flight + '_aft_power spectra')
	plt.plot(np.log10(freqs[mask]), ps_aft, 'b', label='raw')
	plt.plot(np.log10(freqs[mask]), ps_aft_sharpfilter, 'r', label='sharp filter')
	plt.plot(np.log10(freqs[mask]), ps_aft_butterfilter, 'g', label='butter filter')
	plt.legend()
	plt.xlabel("Frequency (Hz)")
	plt.ylabel("Power spectra (dB)")
	plt.title(Flight + '_aft_power spectra')
	ax = plt.axes()
	ax.set_xticks(LogTicks(freqs[mask][0], freqs[mask][-1]), minor=True)
	xticks = [-4, -3, -2, -1., 0., 1., 2.]
	labels = ['$10^{%1.f}$' %x for x in xticks]
	plt.xticks(xticks, labels)
	plt.gcf().set_size_inches((11.5, 8.5), forward=False)
	os.chdir(Output_Directory)
	plt.savefig(Flight + '_aft_power spectra.tiff', bbox_inches="tight", pad_inches=0.06, dpi=200)
	plt.show()
	
	plt.close('all')