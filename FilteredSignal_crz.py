import scipy.io
import os
import numpy as np
from numpy.fft import fft, fftfreq, ifft
import matplotlib.pyplot as plt
from math import pi
import datetime
import matplotlib as mpl
from scipy.signal import butter, lfilter
import sys
import warnings
import pdb

if not sys.warnoptions:
    warnings.simplefilter("ignore")

def sharp_filter(data, lowcut, highcut, Fs):
    data_length = len(data) # number of samples
    freqs = fftfreq(data_length)*Fs # in Hz
    #freqs = (Fs/data_length)*np.arange(1,int(data_length/2)) # calculate manually
    freq_range = np.zeros_like(freqs, dtype=bool)
    for counter, i in enumerate(freqs):
        if i >= lowcut and i <= highcut:
            freq_range[counter] = 1
    spectrum_raw = fft(data)
    spectrum_filtered = spectrum_raw*freq_range
    return ifft(spectrum_filtered)

def butter_bandpass(lowcut, highcut, fs, order=5):
    nyq = 0.5 * fs
    low = lowcut / nyq
    high = highcut / nyq
    b, a = butter(order, [low, high], btype='band')
    return b, a

def butter_bandpass_filter(data, lowcut, highcut, fs, order=5):
    b, a = butter_bandpass(lowcut, highcut, fs, order=order)
    y = lfilter(b, a, data)
    return y

def normal_dist(x , mean , sd, SetMax1):
    prob_density = 1./(sd*np.sqrt(2*np.pi)) * np.exp(-0.5*(((x-mean)/sd)**2))
    if SetMax1 == True:
        prob_density = (prob_density-np.min(prob_density))/(np.max(prob_density)-min(prob_density))
    return prob_density

def LogTicks(a, b):
    ticks = []
    for i in range(int(np.floor(np.log10(a))), int(np.floor(np.log10(b)+1))):
        for j in range(1,10):
            if (10**i)*j >= a and (10**i)*j <=b:
                ticks = ticks + [(10**i)*j]
    return np.log10(ticks)

def WindowAvg (Signal, WindowSize):
    Result = np.convolve(Signal, np.ones((WindowSize,))/WindowSize, mode='valid')
    LeftValue = np.average(Signal[:WindowSize])
    RightValue = np.average(Signal[Signal.size-WindowSize:])
    LeftArray = LeftValue * np.ones((int(WindowSize/2)))
    RightArray = RightValue * np.ones((int((WindowSize-1)/2)))
    Result = np.append(LeftArray, Result)
    Result = np.append(Result, RightArray)
    return Result

def Correlation (A, B, n):
    if n == 0:
        return np.dot(A,B)/(np.linalg.norm(A)*np.linalg.norm(B))
    elif n > 0:
        return np.dot(A[:-n],B[n:])/(np.linalg.norm(A[:-n])*np.linalg.norm(B[n:]))
    elif n < 0:
        n = -n
    return np.dot(A[n:],B[:-n])/(np.linalg.norm(A[n:])*np.linalg.norm(B[:-n]))

def PearsonCorrelation (AA, BB, n):
    # Eq.3 of https://en.wikipedia.org/wiki/Pearson_correlation_coefficient
    A = AA - np.mean(AA)
    B = BB - np.mean(BB)
    if n == 0:
        return np.dot(A,B)/(np.linalg.norm(A)*np.linalg.norm(B))
    elif n > 0:
        return np.dot(A[:-n],B[n:])/(np.linalg.norm(A[:-n])*np.linalg.norm(B[n:]))
    elif n < 0:
        n = -n
    return np.dot(A[n:],B[:-n])/(np.linalg.norm(A[n:])*np.linalg.norm(B[:-n]))
###############################################################################
###############################################################################
# Initial setup

ps_front_min = []
ps_front_max = []
ps_aft_min = []
ps_aft_max = []

#Flight = 'inbound'
#sign = 'A'
#ND = 'off' # Normal_Distribution_Factor
myFlights= ['inbound']      #['inbound', 'outbound']
mySign   = ['B']            #['A', 'B', 'C', 'D']
myND     = ['off', 'on']                #['off', 'on']

for Flight in myFlights:
    for sign in mySign:
        for ND in myND:

            print('processing ' + Flight + '_' + sign + '_' + ND)

            if Flight == 'inbound':
                # Start of the flight: 16:53:54.000
                start_time = 16*3600 + 53*60 + 54 # in seconds

                # Signature Sign
                SS = {'A':0, 'B':1, 'C':2, 'D':3}

                SI = np.zeros((4, 3)) # SI[i,0]: start hour of i-th signature
                                                                                        # SI[i,1]: start minute of i-th signature
                                                                                        # SI[i,2]: time interval of i-th signature in minute

                SI[0, 0] = 18
                SI[0, 1] = 0
                SI[0, 2] = 15

                SI[1, 0] = 19
                SI[1, 1] = 0
                SI[1, 2] = 30

                SI[2, 0] = 20
                SI[2, 1] = 25
                SI[2, 2] = 15

                SI[3, 0] = 21
                SI[3, 1] = 30
                SI[3, 2] = 30

            elif Flight == 'outbound':
                # Start of the flight: 08:14:00.000
                start_time = 8*3600 + 14*60 + 0 # in seconds

                # Signature Sign
                SS = {'A':0, 'B':1, 'C':2, 'D':3}

                SI = np.zeros((4, 3)) # SI[i,0]: start hour of i-th signature
                                                                                        # SI[i,1]: start minute of i-th signature
                                                                                        # SI[i,2]: time interval of i-th signature in minute

                SI[0, 0] = 8
                SI[0, 1] = 50
                SI[0, 2] = 30

                SI[1, 0] = 10
                SI[1, 1] = 5
                SI[1, 2] = 30

                SI[2, 0] = 13
                SI[2, 1] = 15
                SI[2, 2] = 15

                SI[3, 0] = 14
                SI[3, 1] = 0
                SI[3, 2] = 15
            ###############################################################################
            ###############################################################################
                # Load data
            Input_Directory = '/Users/j6hickey/StratodynamicProject/outData/'
            Output_Directory = '/Users/j6hickey/StratodynamicProject/Codes_new/FilteredSignal/' + Flight + '/' + ND +'/'

            os.chdir(Input_Directory)
            time = np.load(Flight + '_follower_1_GMTs.npy')[:, 0]
            VortexAge = np.load(Flight + '_follower_1_VortexAge.npy')[:, 0]
            LatDis = np.load(Flight + '_follower_1_LatDis.npy')[:, 0]
            VerDis = np.load(Flight + '_follower_1_VerDis.npy')[:, 0]
            signal_front_raw = np.load(Flight + '_follower_1_front.npy')[:, 0]
            signal_aft_raw = np.load(Flight + '_follower_1_aft.npy')[:, 0]
            VLF = np.load(Flight + '_follower_1_VLF.npy')[:, 0]
            EDR = np.load(Flight + '_follower_1_EDR.npy')[:, 0]
            Lat = np.load(Flight + '_follower_1_Lat.npy')[:, 0]
            Lon = np.load(Flight + '_follower_1_Lon.npy')[:, 0]
            Alt = np.load(Flight + '_follower_1_Alt.npy')[:, 0]
            Sep = np.load(Flight + '_follower_1_Sep.npy')[:, 0]
            VLF_leader = np.load(Flight + '_leader_1_VLF.npy')[:, 0]
            EDR_leader = np.load(Flight + '_leader_1_EDR.npy')[:, 0]
            Lat_leader = np.load(Flight + '_leader_1_Lat.npy')[:, 0]
            Lon_leader = np.load(Flight + '_leader_1_Lon.npy')[:, 0]
            Alt_leader = np.load(Flight + '_leader_1_Alt.npy')[:, 0]
            ###############################################################################
            ###############################################################################
            # SPL
            Fs = 256 # Frequency of sampling, given in the readme file

            # time variable in this code is considered from the start of the flight
            time = time - time[0]

            # Infrasonic signals (measured in volts) are converted to milli-volts
            signal_front_raw = signal_front_raw * 1000
            signal_aft_raw = signal_aft_raw * 1000

            # Sensitivity of the microphones
            # The sensitivity of 1.5in microphone (installed in the front of the cabin) is 118mV/Pa
            Sensitivity_front = 118
            # The sensitivity of 3in microphone (installed in the aft of the cabin) is 338mV/Pa
            Sensitivity_aft = 338

            # Corresponding pressure perturbation in Pa
            signal_front_raw = signal_front_raw / Sensitivity_front
            signal_aft_raw = signal_aft_raw / Sensitivity_aft

            # Sensitivity of the microphones to low frequencies
            # The sensitivity of 1.5in microphone (installed in the front of the cabin) is 0.1 Hz
            lowcut_front = 0.1
            # The sensitivity of 3in microphone (installed in the aft of the cabin) is 0.001 Hz
            lowcut_aft = 0.001

            # Sharp filtering to keep infrasonic frequencies
            highcut = 20
            signal_front = sharp_filter(signal_front_raw, lowcut_front, highcut, Fs)
            signal_aft = sharp_filter(signal_aft_raw, lowcut_aft, highcut, Fs)

            ## Smooth filtering to keep infrasonic frequencies
            #signal_front = butter_bandpass_filter(signal_front_raw, 1, highcut, Fs, order=5)
            #signal_aft = butter_bandpass_filter(signal_aft_raw, 1, highcut, Fs, order=5)

            # Corresponding pressure perturbation in dB
            # Pressure perturbation is calculated with regard to the mean pressure
            # Negative values are converted to positive values to be able to use log10()
            JPMethod=True #JPH=We need to offset the mean values; Hamid=use absolute
            if JPMethod:
              SPL_front = 20*np.log10((signal_front-np.min(signal_front))/(20e-6))
              SPL_aft = 20*np.log10((signal_aft-np.min(signal_aft))/(20e-6))
            else:
              SPL_front = 20*np.log10(np.abs(signal_front-np.mean(signal_front))/(20e-6))
              SPL_aft = 20*np.log10(np.abs(signal_aft-np.mean(signal_aft))/(20e-6))

            ###############################################################################
            ###############################################################################
            # Options figure
            options=''
            if JPMethod:
                options+='_JPH'


            ###############################################################################
            ###############################################################################
            # Spectrogram
            # Window size is required for PS calculation at each time
            ws = 20 # Window size in seconds
            sihws = int((ws/2)*Fs+0.001) # number of samples in half window size

            n = 2 * sihws # number of samples in a window
            freqs = fftfreq(n)*Fs # in Hz, Fs (which is equal to n/ws) is the sampling frequency in Hz
            mask_front = np.zeros_like(freqs, dtype=bool)
            mask_aft = np.zeros_like(freqs, dtype=bool)
            for counter, j in enumerate(freqs):
                if j >= lowcut_front and j <= highcut:
                    mask_front[counter] = 1
                if j >= lowcut_aft and j <= highcut:
                    mask_aft[counter] = 1

            # Due to computational expense, spectra is considered every 1 s
            slip_time = 1 # In seconds

            dt = 1./Fs

            normalization_factor = normal_dist(np.arange(-sihws+1,sihws+1) , 0 , sihws , True)

            time_range = range(10000, len(time)-10000, int(slip_time/dt))
            Contour_front = np.zeros((len(freqs[mask_front]), len(time_range)))
            Contour_aft = np.zeros((len(freqs[mask_aft]), len(time_range)))
            time = time[time_range]

            for counter, j in enumerate(time_range):

                s = signal_front[j-sihws:j+sihws]
                if ND == 'on':
                    s = np.multiply(s, normalization_factor)

                ps = 2.0*(np.abs(fft(s/(20e-6))/n)**2.0)[mask_front] # The signal over the reference pressure 20e-6Pa
                Contour_front[:, counter] = 10*np.log10(ps) # Convert to decibels

                s = signal_aft[j-sihws:j+sihws]
                if ND == 'on':
                    s = np.multiply(s, normalization_factor)
                ps = 2.0*(np.abs(fft(s/(20e-6))/n)**2.0)[mask_aft] # The signal over the reference pressure 20e-6Pa
                Contour_aft[:, counter] = 10*np.log10(ps) # Convert to decibels; it should be 10*log as it is power

            ps_front_min.append(np.min(Contour_front))
            ps_front_max.append(np.max(Contour_front))
            ps_aft_min.append(np.min(Contour_aft))
            ps_aft_max.append(np.max(Contour_aft))

            plt.figure(Flight + '_' + sign + '_front' + '_spectrogram_' + ND + options)
            X, Y = np.meshgrid(time, np.log10(freqs[mask_front]))

            #plt.contourf(X, Y, Contour_front, 50, cmap='jet') # cmap='RdGy'
            #plt.colorbar(label='Power Spectra (dB)')
            levels = np.arange(-40, 55+1)
            plt.contourf(X, Y, Contour_front, 50, cmap='jet', levels=levels) # cmap='RdGy'
            plt.colorbar(ticks=np.arange(-40, 55+1, 5), label='Power Spectra (dB)')
            plt.xlabel("Time (UTC)")
            plt.ylabel("Frequency (Hz)")
            plt.title(Flight + '_' + sign + '_front' + '_spectrogram_' + ND + options)
            #plt.xlim([time0, time1])
            ax = plt.axes()
            #local_ticks = ax.get_xticks().tolist()
            #xticks = [time0 + m*60 for m in range(int(SI[i, 2])+1)]
            #labels = [str(datetime.timedelta(seconds=(start_time + second)))[:-3] for second in xticks] # [:-3] for removing seconds
            #plt.xticks(xticks, labels)
            plt.xticks(rotation=-90)
            # Cannot use freqs[mask_front] (as the y variable) and then ax.set_yscale("log"),
            # because it takes a long time; so, logarithmic scale is generated manually.
            #yticks = ax.get_yticks().tolist()
            #plt.ylim([np.log10(freqs[mask_front][0]), np.log10(highcut)])
            ax.set_yticks(LogTicks(freqs[mask_front][0], highcut), minor=True)
            yticks = [-1., 0., 1.]
            labels = ['$10^{%1.f}$' %y for y in yticks]
            plt.yticks(yticks, labels)
            #plt.gcf().set_size_inches((11.5, 8.5), forward=False)
            os.chdir(Output_Directory + sign + '/front/')
            plt.savefig(Flight + '_' + sign + '_front' + '_spectrogram_' + ND + options + '.tiff', bbox_inches="tight", pad_inches=0.06, dpi=300)
            #plt.show()

            plt.figure(Flight + '_' + sign + '_aft' + '_spectrogram_' + ND + options)
            X, Y = np.meshgrid(time, np.log10(freqs[mask_aft]))

            #plt.contourf(X, Y, Contour_aft, 50, cmap='jet') # cmap='RdGy'
            #plt.colorbar(label='Power Spectra (dB)')
            levels = np.arange(0, 110+1)
            plt.contourf(X, Y, Contour_aft, 50, cmap='jet', levels=levels) # cmap='RdGy'
            plt.colorbar(ticks=np.arange(0, 110+1, 5), label='Power Spectra (dB)')
            plt.xlabel("Time (UTC)")
            plt.ylabel("Frequency (Hz)")
            plt.title(Flight + '_' + sign + '_aft' + '_spectrogram_' + ND)
            #plt.xlim([time0, time1])
            ax = plt.axes()
            #local_ticks = ax.get_xticks().tolist()
            #xticks = [time0 + m*60 for m in range(int(SI[i, 2])+1)]
            #labels = [str(datetime.timedelta(seconds=(start_time + second)))[:-3] for second in xticks] # [:-3] for removing seconds
            #plt.xticks(xticks, labels)
            plt.xticks(rotation=-90)
            # Cannot use freqs[mask_aft] (as the y variable) and then ax.set_yscale("log"),
            # because it takes a long time; so, logarithmic scale is generated manually.
            #yticks = ax.get_yticks().tolist()
            #plt.ylim([np.log10(freqs[mask_aft][0]), np.log10(highcut)])
            ax.set_yticks(LogTicks(freqs[mask_aft][0], highcut), minor=True)
            yticks = [-1., 0., 1.]
            labels = ['$10^{%1.f}$' %y for y in yticks]
            plt.yticks(yticks, labels)
            plt.gcf().set_size_inches((11.5, 8.5), forward=False)
            os.chdir(Output_Directory + sign + '/aft/')
            plt.savefig(Flight + '_' + sign + '_aft' + '_spectrogram_' + ND + options + '.tiff', bbox_inches="tight", pad_inches=0.06, dpi=300)
            plt.show()
