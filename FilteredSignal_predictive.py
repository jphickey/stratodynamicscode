import scipy.io
import os
import numpy as np
from numpy.fft import fft, fftfreq, ifft
import matplotlib.pyplot as plt
from math import pi
import datetime
import matplotlib as mpl
from scipy.signal import butter, lfilter
import sys
import warnings
import pickle
from scipy.ndimage.filters import gaussian_filter
import pdb
from scipy.signal import find_peaks
from SynchroSqueeze import tf_transforms

if not sys.warnoptions:
    warnings.simplefilter("ignore")


def sharp_filter(data, lowcut, highcut, Fs):
    data_length = len(data) # number of samples
    freqs = fftfreq(data_length)*Fs # in Hz
    #freqs = (Fs/data_length)*np.arange(1,int(data_length/2)) # calculate manually
    freq_range = np.zeros_like(freqs, dtype=bool)
    for counter, i in enumerate(freqs):
        if i >= lowcut and i <= highcut:
            freq_range[counter] = 1
    spectrum_raw = fft(data)
    spectrum_filtered = spectrum_raw*freq_range
    return ifft(spectrum_filtered)

def butter_bandpass(lowcut, highcut, fs, order=5):
    nyq = 0.5 * fs
    low = lowcut / nyq
    high = highcut / nyq
    b, a = butter(order, [low, high], btype='band')
    return b, a

def butter_bandpass_filter(data, lowcut, highcut, fs, order=5):
    b, a = butter_bandpass(lowcut, highcut, fs, order=order)
    y = lfilter(b, a, data)
    return y

def normal_dist(x , mean , sd, SetMax1):
    prob_density = 1./(sd*np.sqrt(2*np.pi)) * np.exp(-0.5*(((x-mean)/sd)**2))
    if SetMax1 == True:
        prob_density = (prob_density-np.min(prob_density))/(np.max(prob_density)-min(prob_density))
    return prob_density

def LogTicks(a, b):
    ticks = []
    for i in range(int(np.floor(np.log10(a))), int(np.floor(np.log10(b)+1))):
        for j in range(1,10):
            if (10**i)*j >= a and (10**i)*j <=b:
                ticks = ticks + [(10**i)*j]
    return np.log10(ticks)

def WindowAvg (Signal, WindowSize):
    Result = np.convolve(Signal, np.ones((WindowSize,))/WindowSize, mode='valid')
    LeftValue = np.average(Signal[:WindowSize])
    RightValue = np.average(Signal[Signal.size-WindowSize:])
    LeftArray = LeftValue * np.ones((int(WindowSize/2)))
    RightArray = RightValue * np.ones((int((WindowSize-1)/2)))
    Result = np.append(LeftArray, Result)
    Result = np.append(Result, RightArray)
    return Result

def Correlation (A, B, n):
    if n == 0:
        return np.dot(A,B)/(np.linalg.norm(A)*np.linalg.norm(B))
    elif n > 0:
        return np.dot(A[:-n],B[n:])/(np.linalg.norm(A[:-n])*np.linalg.norm(B[n:]))
    elif n < 0:
        n = -n
    return np.dot(A[n:],B[:-n])/(np.linalg.norm(A[n:])*np.linalg.norm(B[:-n]))

def PearsonCorrelation (AA, BB, n):
    # Eq.3 of https://en.wikipedia.org/wiki/Pearson_correlation_coefficient
    A = AA - np.mean(AA)
    B = BB - np.mean(BB)
    if n == 0:
        return np.dot(A,B)/(np.linalg.norm(A)*np.linalg.norm(B))
    elif n > 0:
        return np.dot(A[:-n],B[n:])/(np.linalg.norm(A[:-n])*np.linalg.norm(B[n:]))
    elif n < 0:
        n = -n
    return np.dot(A[n:],B[:-n])/(np.linalg.norm(A[n:])*np.linalg.norm(B[:-n]))

def savitzky_golay(y, window_size, order, deriv=0, rate=1):
    r"""Smooth (and optionally differentiate) data with a Savitzky-Golay filter.
    The Savitzky-Golay filter removes high frequency noise from data.
    It has the advantage of preserving the original shape and
    features of the signal better than other types of filtering
    approaches, such as moving averages techniques.
    Parameters
    ----------
    y : array_like, shape (N,)
        the values of the time history of the signal.
    window_size : int
        the length of the window. Must be an odd integer number.
    order : int
        the order of the polynomial used in the filtering.
        Must be less then `window_size` - 1.
    deriv: int
        the order of the derivative to compute (default = 0 means only smoothing)
    Returns
    -------
    ys : ndarray, shape (N)
        the smoothed signal (or it's n-th derivative).
    Notes
    -----
    The Savitzky-Golay is a type of low-pass filter, particularly
    suited for smoothing noisy data. The main idea behind this
    approach is to make for each point a least-square fit with a
    polynomial of high order over a odd-sized window centered at
    the point.
    Examples
    --------
    t = np.linspace(-4, 4, 500)
    y = np.exp( -t**2 ) + np.random.normal(0, 0.05, t.shape)
    ysg = savitzky_golay(y, window_size=31, order=4)
    import matplotlib.pyplot as plt
    plt.plot(t, y, label='Noisy signal')
    plt.plot(t, np.exp(-t**2), 'k', lw=1.5, label='Original signal')
    plt.plot(t, ysg, 'r', label='Filtered signal')
    plt.legend()
    plt.show()
    References
    ----------
    .. [1] A. Savitzky, M. J. E. Golay, Smoothing and Differentiation of
       Data by Simplified Least Squares Procedures. Analytical
       Chemistry, 1964, 36 (8), pp 1627-1639.
    .. [2] Numerical Recipes 3rd Edition: The Art of Scientific Computing
       W.H. Press, S.A. Teukolsky, W.T. Vetterling, B.P. Flannery
       Cambridge University Press ISBN-13: 9780521880688
    """
    import numpy as np
    from math import factorial

    try:
        window_size = np.abs(np.int(window_size))
        order = np.abs(np.int(order))
    except ValueError:
        raise ValueError("window_size and order have to be of type int")
    if window_size % 2 != 1 or window_size < 1:
        raise TypeError("window_size size must be a positive odd number")
    if window_size < order + 2:
        raise TypeError("window_size is too small for the polynomials order")
    order_range = range(order+1)
    half_window = (window_size -1) // 2
    # precompute coefficients
    b = np.mat([[k**i for i in order_range] for k in range(-half_window, half_window+1)])
    m = np.linalg.pinv(b).A[deriv] * rate**deriv * factorial(deriv)
    # pad the signal at the extremes with
    # values taken from the signal itself
    firstvals = y[0] - np.abs( y[1:half_window+1][::-1] - y[0] )
    lastvals = y[-1] + np.abs(y[-half_window-1:-1][::-1] - y[-1])
    y = np.concatenate((firstvals, y, lastvals))
    return np.convolve( m[::-1], y, mode='valid')
###############################################################################
###############################################################################
# Initial setup

ps_front_min = []
ps_front_max = []
ps_aft_min = []
ps_aft_max = []

#Flight = 'inbound'
#sign = 'A'
#ND = 'off' # Normal_Distribution_Factor
myFlights= ['inbound']      #['inbound', 'outbound']
myFlights= ['outbound']
mySign   = ['A','B',"C","D"]#"F","G","H"]            #['A', 'B', 'C', 'D']
myND     = ['off']                #['off', 'on']

for Flight in myFlights:
    for sign in mySign:
        for ND in myND:

            print('processing ' + Flight + '_' + sign + '_' + ND)

            if Flight == 'inbound':
                # Start of the flight: 16:53:54.000
                start_time = 16*3600 + 53*60 + 54 # in seconds

                # Signature Sign
                SS = {'A':0, 'B':1, 'C':2, 'D':3, 'F':4, 'G':5, 'H':6}

                SI = np.zeros((7, 3)) # SI[i,0]: start hour of i-th signature
                                                                                        # SI[i,1]: start minute of i-th signature
                                                                                        # SI[i,2]: time interval of i-th signature in minut
                SI[0, 0] = 18
                SI[0, 1] = 1.5
                SI[0, 2] = 2

                SI[1, 0] = 18
                SI[1, 1] = 5
                SI[1, 2] = 2

                SI[2, 0] = 18
                SI[2, 1] = 9.3
                SI[2, 2] = 2

                SI[3, 0] = 21
                SI[3, 1] = 30
                SI[3, 2] = 30

                SI[4, 0] = 21
                SI[4, 1] = 48
                SI[4, 2] = 2

                SI[5, 0] = 21
                SI[5, 1] = 50
                SI[5, 2] = 3

                SI[6, 0] = 21
                SI[6, 1] = 58
                SI[6, 2] = 2

            elif Flight == 'outbound':
                # Start of the flight: 08:14:00.000
                start_time = 8*3600 + 14*60 + 0 # in seconds

                # Signature Sign
                SS = {'A':0, 'B':1, 'C':2, 'D':3}

                SI = np.zeros((4, 3)) # SI[i,0]: start hour of i-th signature
                                                                                        # SI[i,1]: start minute of i-th signature
                                                                                        # SI[i,2]: time interval of i-th signature in minute
                SI[0, 0] = 8
                SI[0, 1] = 50
                SI[0, 2] = 30

                SI[1, 0] = 10
                SI[1, 1] = 5
                SI[1, 2] = 30

                SI[2, 0] = 13
                SI[2, 1] = 15
                SI[2, 2] = 15

                SI[3, 0] = 14
                SI[3, 1] = 10
                SI[3, 2] = 3
            ###############################################################################
            ###############################################################################
                # Load data
            Input_Directory = '/Users/j6hickey/StratodynamicProject/outData/'
            Output_Directory = '/Users/j6hickey/StratodynamicProject/Codes/FilteredSignal/' + Flight + '/' + ND +'/'

            os.chdir(Input_Directory)
            time = np.load(Flight + '_follower_GMTs.npy')[:, 0]
            VortexAge = np.load(Flight + '_follower_VortexAge.npy')[:, 0]
            LatDis = np.load(Flight + '_follower_LatDis.npy')[:, 0]
            VerDis = np.load(Flight + '_follower_VerDis.npy')[:, 0]
            signal_front_raw = np.load(Flight + '_follower_front.npy')[:, 0]
            signal_aft_raw = np.load(Flight + '_follower_aft.npy')[:, 0]
            VLF = np.load(Flight + '_follower_VLF.npy')[:, 0]
            EDR = np.load(Flight + '_follower_EDR.npy')[:, 0]
            Lat = np.load(Flight + '_follower_Lat.npy')[:, 0]
            Lon = np.load(Flight + '_follower_Lon.npy')[:, 0]
            Alt = np.load(Flight + '_follower_Alt.npy')[:, 0]
            Sep = np.load(Flight + '_follower_Sep.npy')[:, 0]
            VLF_leader = np.load(Flight + '_leader_VLF.npy')[:, 0]
            EDR_leader = np.load(Flight + '_leader_EDR.npy')[:, 0]
            Lat_leader = np.load(Flight + '_leader_Lat.npy')[:, 0]
            Lon_leader = np.load(Flight + '_leader_Lon.npy')[:, 0]
            Alt_leader = np.load(Flight + '_leader_Alt.npy')[:, 0]
            ###############################################################################
            ###############################################################################
            # SPL
            Fs = 256 # Frequency of sampling, given in the readme file

            # time variable in this code is considered from the start of the flight
            time = time - time[0]

            # Infrasonic signals (measured in volts) are converted to milli-volts
            signal_front_raw = signal_front_raw * 1000
            signal_aft_raw = signal_aft_raw * 1000

            # Sensitivity of the microphones
            # The sensitivity of 1.5in microphone (installed in the front of the cabin) is 118mV/Pa
            Sensitivity_front = 118
            # The sensitivity of 3in microphone (installed in the aft of the cabin) is 338mV/Pa
            Sensitivity_aft = 338

            # Corresponding pressure perturbation in Pa
            signal_front_raw = signal_front_raw / Sensitivity_front
            signal_aft_raw = signal_aft_raw / Sensitivity_aft

            # Sensitivity of the microphones to low frequencies
            # The sensitivity of 1.5in microphone (installed in the front of the cabin) is 0.1 Hz
            lowcut_front = 0.1
            # The sensitivity of 3in microphone (installed in the aft of the cabin) is 0.001 Hz
            lowcut_aft = 0.001

            # Sharp filtering to keep infrasonic frequencies
            highcut = 20
            signal_front = sharp_filter(signal_front_raw, lowcut_front, highcut, Fs)
            signal_aft = sharp_filter(signal_aft_raw, lowcut_aft, highcut, Fs)

            ## Smooth filtering to keep infrasonic frequencies
            #signal_front = butter_bandpass_filter(signal_front_raw, 1, highcut, Fs, order=5)
            #signal_aft = butter_bandpass_filter(signal_aft_raw, 1, highcut, Fs, order=5)

            # Corresponding pressure perturbation in dB
            # Pressure perturbation is calculated with regard to the mean pressure
            # Negative values are converted to positive values to be able to use log10()
            JPMethod=False #JPH=We need to offset the mean values; Hamid=use absolute
            if JPMethod:
              SPL_front = 20*np.log10((signal_front-np.min(signal_front))/(20e-6))
              SPL_aft = 20*np.log10((signal_aft-np.min(signal_aft))/(20e-6))
            else:
              SPL_front = 20*np.log10(np.abs(signal_front-np.mean(signal_front))/(20e-6))
              SPL_aft = 20*np.log10(np.abs(signal_aft-np.mean(signal_aft))/(20e-6))

            ###############################################################################
            ###############################################################################
            # Options figure
            removeBase =False  # Remove the baseline state from nofellofly at each frequency
            removeMean =True  # Remove the mean at each frequency

            options=''
            if JPMethod:
                options+='_JPH'
            if removeBase:
                options+='_removeBase'
                baseFreq_front = open('/Users/j6hickey/StratodynamicProject/Codes_Nofellofly/FilteredSignal/Flight_I/off/A/front/Flight_I_A_front_spectrogram_off.pickle','rb')
                baseFreq_aft = open('/Users/j6hickey/StratodynamicProject/Codes_Nofellofly/FilteredSignal/Flight_I/off/A/aft/Flight_I_A_aft_spectrogram_off.tiff.pickle','rb')
            elif removeMean:
                options+='_focused'

            ###############################################################################
            ###############################################################################
            # Pressure mean value
            os.chdir('/Users/j6hickey/StratodynamicProject/Codes/FilteredSignal/' + Flight + '/')
            PMV = open(Flight + '_PressureMeanValue.dat','w')
            PMV.write('Pressure mean value for the front microphone\n' + str(np.mean(signal_front).real) + '\n')
            PMV.write('Pressure mean value for the aft microphone\n' + str(np.mean(signal_aft).real) + '\n')
            PMV.close()
            ###############################################################################
            ###############################################################################
            # Plot power spectra (power vs frequency)
            i = SS[sign]

            time0 = SI[i, 0]*3600 + SI[i, 1]*60 - start_time # seconds passed from the start_time
            time1 = time0 + SI[i, 2]*60 # seconds passed from the start_time

            index0 = int(time0 * Fs)
            index1 = int(time1 * Fs)

            n = time[index0:index1+1].shape[0] # number of samples
            freqs = fftfreq(n)*Fs # in Hz, Fs is the sampling frequency in Hz
            mask = freqs > 0
            #dt = 1./Fs
            #freqs_manual = (1./(dt*n))*np.arange(1,int(n/2))

            ps_front_raw = 2.0*(np.abs(fft(signal_front_raw[index0:index1+1]/(20e-6))/n)**2.0)[mask]
            ps_aft_raw = 2.0*(np.abs(fft(signal_aft_raw[index0:index1+1]/(20e-6))/n)**2.0)[mask]

            ps_front = 2.0*(np.abs(fft(signal_front[index0:index1+1]/(20e-6))/n)**2.0)[mask]
            ps_aft = 2.0*(np.abs(fft(signal_aft[index0:index1+1]/(20e-6))/n)**2.0)[mask]

            # front_spectra
            plt.figure(Flight + '_' + sign + '_front_power spectra')
            plt.plot(np.log10(freqs[mask]), ps_front_raw, 'b', label='raw')
#                       plt.plot(np.log10(freqs[mask]), ps_front, 'r', label='sharp filter')
#                       plt.legend()
            plt.xlabel("Frequency (Hz)")
            plt.ylabel("Power spectra (dB)")
            plt.title(Flight + '_' + sign + '_front_power spectra')
            ax = plt.axes()
            ax.set_xticks(LogTicks(freqs[mask][0], freqs[mask][-1]), minor=True)
            xticks = [-3, -2, -1., 0., 1., 2.]
            labels = ['$10^{%1.f}$' %x for x in xticks]
            plt.xticks(xticks, labels)
            plt.gcf().set_size_inches((11.5, 8.5), forward=False)
            os.chdir(Output_Directory + sign + '/')
            plt.savefig(Flight + '_' + sign + '_front_power spectra.tiff', bbox_inches="tight", pad_inches=0.06, dpi=200)
            #plt.show()

            # aft_spectra
            plt.figure(Flight + '_' + sign + '_aft_power spectra')
            plt.plot(np.log10(freqs[mask]), ps_aft_raw, 'b', label='raw')
#                       plt.plot(np.log10(freqs[mask]), ps_aft, 'r', label='sharp filter')
#                       plt.legend()
            plt.xlabel("Frequency (Hz)")
            plt.ylabel("Power spectra (dB)")
            plt.title(Flight + '_' + sign + '_aft_power spectra')
            ax = plt.axes()
            ax.set_xticks(LogTicks(freqs[mask][0], freqs[mask][-1]), minor=True)
            xticks = [-3, -2, -1., 0., 1., 2.]
            labels = ['$10^{%1.f}$' %x for x in xticks]
            plt.xticks(xticks, labels)
            plt.gcf().set_size_inches((11.5, 8.5), forward=False)
            os.chdir(Output_Directory + sign + '/')
            plt.savefig(Flight + '_' + sign + '_aft_power spectra.tiff', bbox_inches="tight", pad_inches=0.06, dpi=200)
            #plt.show()

            ############################################
            #ssqueezepy
            ############################################
            plt.figure(Flight + '_' + sign + '_SSqueeze')
            plt.close('all')
            def viz(x, Tx, Wx):
                pdb.set_trace()
                nx,ny=np.shape(Wx)
                smean=np.mean(Wx,axis=1)
                for ii in range(nx):
                    Wx[ii,:]-=smean[ii]
                plt.imshow(np.abs(Wx), aspect='auto', cmap='turbo')
                plt.figure()
                plt.imshow(np.abs(Tx), aspect='auto', vmin=0, vmax=.2, cmap='turbo')
                plt.show()
            from ssqueezepy import ssq_cwt, ssq_stft
            s = signal_aft[index0:index1]
            myTime=time[index0:index1]
            myTime=range(index0,index1)
            #tf_transforms(s, myTime, n_ridges=3, padtype='wrap', stft_bw=4, ssq_stft_bw=4, penalty=20)

            ############################################
            #ssqueezepy (END)
            ############################################



            ###############################################################################
            ###############################################################################
            # Spectrogram
            # Window size is required for PS calculation at each time
            ws = 20 # Window size in seconds
            sihws = int((ws/2)*Fs+0.001) # number of samples in half window size

            n = 2 * sihws # number of samples in a window
            freqs = fftfreq(n)*Fs # in Hz, Fs (which is equal to n/ws) is the sampling frequency in Hz
            mask_front = np.zeros_like(freqs, dtype=bool)
            mask_aft = np.zeros_like(freqs, dtype=bool)
            for counter, j in enumerate(freqs):
                if j >= lowcut_front and j <= highcut:
                    mask_front[counter] = 1
                if j >= lowcut_aft and j <= highcut:
                    mask_aft[counter] = 1

            # Due to computational expense, spectra is considered every 1 s
            slip_time = 1 # In seconds

            dt = 1./Fs

            normalization_factor = normal_dist(np.arange(-sihws+1,sihws+1) , 0 , sihws , True)

            time_range = range(index0+2*sihws, index1-sihws+1, int(slip_time/dt))
            Contour_front = np.zeros((len(freqs[mask_front]), len(time_range)))
            Contour_aft = np.zeros((len(freqs[mask_aft]), len(time_range)))
            fft_front = np.zeros((len(freqs[mask_front]), len(time_range)))
            fft_aft = np.zeros((len(freqs[mask_aft]), len(time_range)))
            time = time[time_range]

            for counter, j in enumerate(time_range):
                # Biassed filtering
                s = signal_front[j-2*sihws:j]
                if ND == 'on':
                    s = np.multiply(s, normalization_factor)
                ps = 2.0*(np.abs(fft(s/(20e-6))/n)**2.0)[mask_front] # The signal over the reference pressure 20e-6Pa
                Contour_front[:, counter] = 10*np.log10(ps) # Convert to decibels
                fft_aft[:, counter] =  (np.abs(fft(s)/n)**2)[mask_aft]

                s = signal_aft[j-sihws:j+sihws]
                if ND == 'on':
                    s = np.multiply(s, normalization_factor)
                ps = 2.0*(np.abs(fft(s/(20e-6))/n)**2.0)[mask_aft] # The signal over the reference pressure 20e-6Pa
                Contour_aft[:, counter] = 10*np.log10(ps) # Convert to decibels; it should be 10*log as it is power
                fft_aft[:, counter] = (np.abs(fft(s)/n)**2)[mask_aft]

            ps_front_min.append(np.min(Contour_front))
            ps_front_max.append(np.max(Contour_front))
            ps_aft_min.append(np.min(Contour_aft))
            ps_aft_max.append(np.max(Contour_aft))
            levels = np.arange(-40, 55+1)
            #Remove the known baseline frequencies in aircraft
            if removeBase:
                levels = np.arange(-5, 15+1)
                data = pickle.load(baseFreq_front)
                XX=data['X']
                YY=data['Y']
                baseContour=data['Countour']
                avgFrq=np.mean(baseContour,axis=1)
                nx,ny=np.shape(Contour_front)
                for ii in range(ny):
                    Contour_front[:,ii]-=avgFrq
            elif removeMean:
                levels = np.arange(0, 20+1)
                avgFrq=np.mean(Contour_front,axis=1)
                avgFrq_fft=np.mean(fft_front,axis=1)
                nx,ny=np.shape(Contour_front)
                for ii in range(ny):
                    Contour_front[:,ii]-=avgFrq
                    fft_front[:, ii]-=avgFrq_fft
                Contour_front=gaussian_filter(Contour_front, sigma=5)
                nnx,nny=np.shape(Contour_front)
                sumFreq=np.mean(Contour_front[:int(3*nnx/4),:],axis=0)



            plt.figure(Flight + '_' + sign + '_front' + '_spectrogram_' + ND + options)
            X, Y = np.meshgrid(time, np.log10(freqs[mask_front]))
            plt.contourf(X, Y,  Contour_front, 50, cmap='jet', levels=levels,alpha=0.3) # cmap='RdGy'
            if removeMean:
              FilterOn=True

              vor_dis = np.sqrt((LatDis**2)+(VerDis**2))[time_range]
              vor_dis_ND=0.02+abs((vor_dis-min(vor_dis))/(max(vor_dis)-min(vor_dis))-1)  #Inverse distance
              sumFreq_ND=(sumFreq-min(sumFreq))/(max(sumFreq)-min(sumFreq))+0.02
              EDR_leader_ND=(EDR_leader[time_range]-min(EDR_leader[time_range]))/(max(EDR_leader[time_range])-min(EDR_leader[time_range]))+0.02
              EDR_ND=(EDR[time_range]-min(EDR[time_range]))/(max(EDR[time_range])-min(EDR[time_range]))+0.02
              VLF_sign=(VLF[time_range]-min(VLF[time_range]))/(max(VLF[time_range])-min(VLF[time_range]))+0.02
              VLF_sign=sharp_filter(VLF_sign, lowcut_aft, highcut, Fs)
              posVals_std_f=sumFreq_ND-0.02>np.std(sumFreq_ND)
              posVals_mean_f=sumFreq_ND-0.02>np.mean(sumFreq_ND)
              posVals_std_d=vor_dis_ND-0.02>np.std(vor_dis_ND)
              posVals_mean_d=vor_dis_ND-0.02>np.mean(vor_dis_ND)
              posVals_std_vlf=vor_dis_ND-0.02>np.std(VLF_sign)
              posVals_mean_vlf=vor_dis_ND-0.02>np.mean(VLF_sign)
              print('----Front mic-----')
              print('distance <-> frequency')
              print('correlation (mean)',sum(posVals_mean_f*posVals_mean_d)/len(posVals_mean_f))
              print('correlation (std)',sum(posVals_std_f*posVals_std_d)/len(posVals_mean_f))
              print('VLF <-> frequency')
              print('correlation (mean)',sum(posVals_mean_f*posVals_mean_vlf)/len(posVals_mean_f))
              print('correlation (std)',sum(posVals_std_f*posVals_std_vlf)/len(posVals_mean_f))
              if FilterOn:
                  vor_dis_ND=savitzky_golay(vor_dis_ND,121,3)
                  sumFreq_ND=savitzky_golay(sumFreq_ND,121,3)
                  EDR_leader_ND=savitzky_golay(EDR_leader_ND,21,3)
              peaks, _ = find_peaks(sumFreq_ND, height=0,distance=50)
              peaks_EDR,_=find_peaks(EDR_leader_ND, height=0,distance=50)
              peaks_vor,_=find_peaks(vor_dis_ND, height=0,distance=100)
              peaks_vlf,_=find_peaks(VLF_sign, height=0,distance=100)
              overlay=True
              if overlay:
                plt.plot(time,vor_dis_ND,'k:')
                plt.plot(time,EDR_leader_ND,'k')
                plt.plot(time,sumFreq_ND,'r')
                plt.plot(time[peaks], sumFreq_ND[peaks], "ro")
                plt.plot(time[peaks_EDR], EDR_leader_ND[peaks_EDR], "ko")
                plt.plot(time[peaks_vor], vor_dis_ND[peaks_vor], "ko",alpha=0.5)
                print('Correlation EDR/distance:', PearsonCorrelation(sumFreq_ND,EDR_leader_ND,0),PearsonCorrelation(sumFreq_ND,vor_dis_ND,0))
                for ii in peaks:
                   plt.plot([time[ii],time[ii]],[-1,1], "k:",lw=0.75)

              #pdb.set_trace()
            plt.colorbar(ticks=np.arange(-40, 55+1, 5), label='Power Spectra (dB)')
            plt.xlabel("Time (UTC)")
            plt.ylabel("Frequency (Hz)")
            plt.title(Flight + '_' + sign + '_front' + '_spectrogram_' + ND + options)
            plt.xlim([time0, time1])
            ax = plt.axes()
            #local_ticks = ax.get_xticks().tolist()
            xticks = [time0 + m*60 for m in range(int(SI[i, 2])+1)]
            labels = [str(datetime.timedelta(seconds=(start_time + second)))[:-3] for second in xticks] # [:-3] for removing seconds
            plt.xticks(xticks, labels)
            plt.xticks(rotation=-90)
            # Cannot use freqs[mask_front] (as the y variable) and then ax.set_yscale("log"),
            # because it takes a long time; so, logarithmic scale is generated manually.
            #yticks = ax.get_yticks().tolist()
            #plt.ylim([np.log10(freqs[mask_front][0]), np.log10(highcut)])
            ax.set_yticks(LogTicks(freqs[mask_front][0], highcut), minor=True)
            yticks = [-1., 0., 1.]
            labels = ['$10^{%1.f}$' %y for y in yticks]
            plt.yticks(yticks, labels)
            #plt.gcf().set_size_inches((11.5, 8.5), forward=False)
            os.chdir(Output_Directory + sign + '/front/')
            plt.savefig(Flight + '_' + sign + '_front' + '_spectrogram_' + ND + options + '.tiff', bbox_inches="tight", pad_inches=0.06, dpi=300)
            #plt.show()

            plt.figure(Flight + '_' + sign + '_aft' + '_spectrogram_' + ND + options)
            X, Y = np.meshgrid(time, np.log10(freqs[mask_aft]))

            #plt.contourf(X, Y, Contour_aft, 50, cmap='jet') # cmap='RdGy'
            #plt.colorbar(label='Power Spectra (dB)')
            levels = np.arange(0, 110+1)
            #Remove the known baseline frequencies in aircraft
            if removeBase:
                levels = np.arange(-10, 10)
                data = pickle.load(baseFreq_aft)
                XX=data['X']
                YY=data['Y']
                baseContour=data['Countour']
                avgFrq=np.mean(baseContour,axis=1)
                nx,ny=np.shape(Contour_aft)
                for ii in range(ny):
                    Contour_aft[:,ii]-=avgFrq
            if removeMean:
                levels = np.arange(0, 15)
                avgFrq=np.mean(Contour_aft,axis=1)
                avgFrq_fft=np.mean(fft_aft,axis=1)
                nx,ny=np.shape(Contour_aft)
                for ii in range(ny):
                    Contour_aft[:,ii]-=avgFrq
                    fft_aft[:,ii]-=avgFrq_fft
                Contour_aft=gaussian_filter( Contour_aft, sigma=5)
                nnx,nny=np.shape(Contour_aft)
                sumFreq=np.mean(Contour_aft[:int(nnx/20),:],axis=0)
                #sumFreq=np.mean(Contour_aft,axis=0)



            plt.contourf(X, Y, Contour_aft, 50, cmap='jet', levels=levels,alpha=0.35) # cmap='RdGy'
            if removeMean:
              vor_dis = np.sqrt((LatDis**2)+(VerDis**2))[time_range]
              vor_dis_ND=0.02+abs((vor_dis-min(vor_dis))/(max(vor_dis)-min(vor_dis))-1)  #Inverse distance
              sumFreq_ND=(sumFreq-min(sumFreq))/(max(sumFreq)-min(sumFreq))+0.02
              VLF_sign=sharp_filter(VLF, lowcut_aft, highcut, Fs)
              VLF_sign=(VLF_sign[time_range]-min(VLF_sign[time_range]))/(max(VLF_sign[time_range])-min(VLF_sign[time_range]))+0.02
              #
              EDR_leader_ND=(EDR_leader[time_range]-min(EDR_leader[time_range]))/(max(EDR_leader[time_range])-min(EDR_leader[time_range]))+0.02
              peaks, _ = find_peaks(sumFreq_ND, height=0,distance=50,prominence=0.15)
              peaks_EDR,_=find_peaks(EDR_leader_ND, height=0,distance=50)
              peaks_vor,_=find_peaks(vor_dis_ND, height=0,distance=100)
              peaks_vlf,_=find_peaks(VLF_sign, height=0,distance=100)
              posVals_std_f=sumFreq_ND-0.02>np.std(sumFreq_ND)
              posVals_mean_f=sumFreq_ND-0.02>np.mean(sumFreq_ND)
              posVals_std_d=vor_dis_ND-0.02>np.std(vor_dis_ND)
              posVals_mean_d=vor_dis_ND-0.02>np.mean(vor_dis_ND)
              posVals_std_vlf=vor_dis_ND-0.02>np.std(VLF_sign)
              posVals_mean_vlf=vor_dis_ND-0.02>np.mean(VLF_sign)
              print('----Aft mic-----')
              print('distance <-> frequency')
              print('correlation (mean)',sum(posVals_mean_f*posVals_mean_d)/len(posVals_mean_f))
              print('correlation (std)',sum(posVals_std_f*posVals_std_d)/len(posVals_mean_f))
              print('VLF <-> frequency')
              print('correlation (mean)',sum(posVals_mean_f*posVals_mean_vlf)/len(posVals_mean_f))
              print('correlation (std)',sum(posVals_std_f*posVals_std_vlf)/len(posVals_mean_f))

              if overlay:
                plt.plot(time,vor_dis_ND,'k:')
                plt.plot(time,(EDR_leader[time_range]-min(EDR_leader[time_range]))/(max(EDR_leader[time_range])-min(EDR_leader[time_range]))+0.02,'k')
                plt.plot(time,sumFreq_ND,'r')
                plt.plot(time,VLF_sign,'b',lw=2)
                plt.plot(time[peaks], sumFreq_ND[peaks], "ro")
                plt.plot(time[peaks_EDR], EDR_leader_ND[peaks_EDR], "ko")
                plt.plot(time[peaks_vor], vor_dis_ND[peaks_vor], "ko",alpha=0.5)
                plt.plot(time[peaks_vlf], VLF_sign[peaks_vlf], "bo",alpha=0.5)
                plt.plot([time[0],time[-1]],[Y[int(nnx/20),0],Y[int(nnx/20),0]],"k:")
                for ii in peaks:
                  plt.plot([time[ii],time[ii]],[-1,1], "k:",lw=0.75)

                plt.figure()
                plt.plot(time,sumFreq_ND,'r',lw=2,label="infrasound")
                plt.plot(time,VLF_sign,'b',label="VLF",alpha=0.5)
                plt.plot(time[peaks], sumFreq_ND[peaks], "ro")
                plt.plot(time[peaks_vlf], VLF_sign[peaks_vlf], "bo",alpha=0.5)
                plt.legend()
                plt.xlabel ("time (UTH)")
                plt.ylabel ("normalized signals")
                plt.savefig(Flight + '_' + sign + '_aft' + '_signal_' + ND + options + '.tiff', bbox_inches="tight", pad_inches=0.06, dpi=300)

                plt.close()


            plt.colorbar(ticks=np.arange(0, 110+1, 5), label='Power Spectra (dB)')
            plt.xlabel("Time (UTC)")
            plt.ylabel("Frequency (Hz)")
            plt.title(Flight + '_' + sign + '_aft' + '_spectrogram_' + ND)
            plt.xlim([time0, time1])
            ax = plt.axes()
            #local_ticks = ax.get_xticks().tolist()
            xticks = [time0 + m*60 for m in range(int(SI[i, 2])+1)]
            labels = [str(datetime.timedelta(seconds=(start_time + second)))[:-3] for second in xticks] # [:-3] for removing seconds
            plt.xticks(xticks, labels)
            plt.xticks(rotation=-90)
            # Cannot use freqs[mask_aft] (as the y variable) and then ax.set_yscale("log"),
            # because it takes a long time; so, logarithmic scale is generated manually.
            #yticks = ax.get_yticks().tolist()
            #plt.ylim([np.log10(freqs[mask_aft][0]), np.log10(highcut)])
            ax.set_yticks(LogTicks(freqs[mask_aft][0], highcut), minor=True)
            yticks = [-1., 0., 1.]
            labels = ['$10^{%1.f}$' %y for y in yticks]
            plt.yticks(yticks, labels)
            plt.gcf().set_size_inches((11.5, 8.5), forward=False)
            os.chdir(Output_Directory + sign + '/aft/')
            plt.savefig(Flight + '_' + sign + '_aft' + '_spectrogram_' + ND + options + '.tiff', bbox_inches="tight", pad_inches=0.06, dpi=300)

            for ii in peaks:
                plt.figure(Flight + '_' + sign + '_front' + '_peakSpectrum')
                plt.gcf().set_size_inches((11.5, 8.5), forward=False)
                yticks = [-1., 0., 1.]
                labels = ['$10^{%1.f}$' %y for y in yticks]
                plt.xticks(yticks, labels)
                plt.xlabel("Frequency (Hz)")
                plt.plot(np.log10(freqs[mask_aft]),(fft_aft[:,ii]))
            plt.savefig(Flight + '_' + sign + '_aft' + '_peakSpectrum_' + ND + options + '.tiff', bbox_inches="tight", pad_inches=0.06, dpi=300)


            #plt.show()
            ###############################################################################
            ###############################################################################
            # Plot diagrams
            # SPL
            SPL_front = SPL_front[time_range]
            SPL_aft = SPL_aft[time_range]

            plt.figure(Flight + '_' + sign + '_front' + '_SPL')
            plt.plot(time, SPL_front)
            plt.plot(time, WindowAvg(SPL_front, 20), 'r')
            plt.xlabel("Time (UTC)")
            plt.ylabel("SPL (dB)")
            plt.title(Flight + '_' + sign + '_front' + '_SPL')
            plt.xlim([time0, time1])
            ax = plt.axes()
            xticks = [time0 + m*60 for m in range(int(SI[i, 2])+1)]
            labels = [str(datetime.timedelta(seconds=(start_time + second)))[:-3] for second in xticks] # [:-3] for removing seconds
            plt.xticks(xticks, labels)
            plt.xticks(rotation=-90)
            plt.gcf().set_size_inches((11.5, 8.5), forward=False)
            os.chdir(Output_Directory + sign + '/front/')
            plt.savefig(Flight + '_' + sign + '_front' + '_SPL' + '.tiff', bbox_inches="tight", pad_inches=0.06, dpi=300)
            #plt.show()

            plt.figure(Flight + '_' + sign + '_aft' + '_SPL')
            plt.plot(time, SPL_aft)
            plt.plot(time, WindowAvg(SPL_aft, 20), 'r')
            plt.xlabel("Time (UTC)")
            plt.ylabel("SPL (dB)")
            plt.title(Flight + '_' + sign + '_aft' + '_SPL')
            plt.xlim([time0, time1])
            ax = plt.axes()
            xticks = [time0 + m*60 for m in range(int(SI[i, 2])+1)]
            labels = [str(datetime.timedelta(seconds=(start_time + second)))[:-3] for second in xticks] # [:-3] for removing seconds
            plt.xticks(xticks, labels)
            plt.xticks(rotation=-90)
            plt.gcf().set_size_inches((11.5, 8.5), forward=False)
            os.chdir(Output_Directory + sign + '/aft/')
            plt.savefig(Flight + '_' + sign + '_aft' + '_SPL' + options+ '.tiff', bbox_inches="tight", pad_inches=0.06, dpi=300)
            #plt.show()

            # Spectrum at a specific frequency
            freq = 0.5

            index_freq_front = (np.abs(freqs[mask_front] - freq)).argmin()
            spectrum_front = Contour_front[index_freq_front,:]

            index_freq_aft = (np.abs(freqs[mask_aft] - freq)).argmin()
            spectrum_aft = Contour_aft[index_freq_aft,:]

            plt.figure(Flight + '_' + sign + '_front' + '_spectrum_freq' + str(freq) + '_' + ND)
            plt.plot(time, spectrum_front)
            plt.plot(time, WindowAvg(spectrum_front, 20), 'r')
            plt.xlabel("Time (UTC)")
            plt.ylabel("Power (dB)")
            plt.title(Flight + '_' + sign + '_front' + '_spectrum_freq' + str(freq) + '_' + ND)
            plt.xlim([time0, time1])
            ax = plt.axes()
            xticks = [time0 + m*60 for m in range(int(SI[i, 2])+1)]
            labels = [str(datetime.timedelta(seconds=(start_time + second)))[:-3] for second in xticks] # [:-3] for removing seconds
            plt.xticks(xticks, labels)
            plt.xticks(rotation=-90)
            plt.gcf().set_size_inches((11.5, 8.5), forward=False)
            os.chdir(Output_Directory + sign + '/front/')
            plt.savefig(Flight + '_' + sign + '_front' + '_spectrum_freq' + str(freq) + '_' + ND + '.tiff', bbox_inches="tight", pad_inches=0.06, dpi=300)
            #plt.show()

            plt.figure(Flight + '_' + sign + '_aft' + '_spectrum_freq' + str(freq) + '_' + ND)
            plt.plot(time, spectrum_aft)
            plt.plot(time, WindowAvg(spectrum_aft, 20), 'r')
            plt.xlabel("Time (UTC)")
            plt.ylabel("Power (dB)")
            plt.title(Flight + '_' + sign + '_aft' + '_spectrum_freq' + str(freq) + '_' + ND)
            plt.xlim([time0, time1])
            ax = plt.axes()
            xticks = [time0 + m*60 for m in range(int(SI[i, 2])+1)]
            labels = [str(datetime.timedelta(seconds=(start_time + second)))[:-3] for second in xticks] # [:-3] for removing seconds
            plt.xticks(xticks, labels)
            plt.xticks(rotation=-90)
            plt.gcf().set_size_inches((11.5, 8.5), forward=False)
            os.chdir(Output_Directory + sign + '/aft/')
            plt.savefig(Flight + '_' + sign + '_aft' + '_spectrum_freq' + str(freq) + '_' + ND + '.tiff', bbox_inches="tight", pad_inches=0.06, dpi=300)
            #plt.show()

            # Vortex distance and vortex age
            vor_dis = np.sqrt((LatDis**2)+(VerDis**2))[time_range]
            VortexAge = VortexAge[time_range]
            plt.figure(Flight + '_' + sign + '_vortex-distance-age')
            plt.plot(time, vor_dis, 'b')
            plt.xlabel("Time (UTC)")
            plt.ylabel("Vortex distance (m)", color="blue")
            plt.title(Flight + '_' + sign + '_vortex-distance-age')
            plt.xlim([time0, time1])
            ax = plt.axes()
            xticks = [time0 + m*60 for m in range(int(SI[i, 2])+1)]
            labels = [str(datetime.timedelta(seconds=(start_time + second)))[:-3] for second in xticks] # [:-3] for removing seconds
            plt.xticks(xticks, labels)
            plt.xticks(rotation=-90)
            ax2=ax.twinx()
            ax2.plot(time, VortexAge, 'r')
            ax2.set_ylabel("Vortex age (s)", color="red")
            plt.gcf().set_size_inches((11.5, 8.5), forward=False)
            os.chdir(Output_Directory + sign + '/')
            plt.savefig(Flight + '_' + sign + '_vortex-distance-age' + '.tiff', bbox_inches="tight", pad_inches=0.06, dpi=300)
            #plt.show()

            # Separation and vortex age
            plt.figure(Flight + '_' + sign + '_separation-age')
            plt.plot(time, Sep[time_range], 'b')
            plt.xlabel("Time (UTC)")
            plt.ylabel("Separation (m)", color="blue")
            plt.title(Flight + '_' + sign + '_separation-age')
            plt.xlim([time0, time1])
            ax = plt.axes()
            xticks = [time0 + m*60 for m in range(int(SI[i, 2])+1)]
            labels = [str(datetime.timedelta(seconds=(start_time + second)))[:-3] for second in xticks] # [:-3] for removing seconds
            plt.xticks(xticks, labels)
            plt.xticks(rotation=-90)
            ax2=ax.twinx()
            ax2.plot(time, VortexAge, 'r')
            ax2.set_ylabel("Vortex age (s)", color="red")
            plt.gcf().set_size_inches((11.5, 8.5), forward=False)
            os.chdir(Output_Directory + sign + '/')
            plt.savefig(Flight + '_' + sign + '_separation-age' + '.tiff', bbox_inches="tight", pad_inches=0.06, dpi=300)
            #plt.show()

            # VLF, EDR, Lat, Lon, Alt
            plt.figure(Flight + '_' + sign + '_VLF')
            plt.plot(time, VLF[time_range], 'b', label='follower')
            plt.plot(time, VLF_leader[time_range], 'r', label='leader')
            plt.legend()
            #plt.plot(time, WindowAvg(VLF[time_range], 20), 'r')
            plt.xlabel("Time (UTC)")
            plt.ylabel("Vertical load factor (g)")
            plt.title(Flight + '_' + sign + '_VLF')
            plt.xlim([time0, time1])
            ax = plt.axes()
            xticks = [time0 + m*60 for m in range(int(SI[i, 2])+1)]
            labels = [str(datetime.timedelta(seconds=(start_time + second)))[:-3] for second in xticks] # [:-3] for removing seconds
            plt.xticks(xticks, labels)
            plt.xticks(rotation=-90)
            plt.gcf().set_size_inches((11.5, 8.5), forward=False)
            os.chdir(Output_Directory + sign + '/')
            plt.savefig(Flight + '_' + sign + '_VLF' + '.tiff', bbox_inches="tight", pad_inches=0.06, dpi=300)
            #plt.show()

            plt.figure(Flight + '_' + sign + '_EDR')
            plt.plot(time, EDR[time_range], 'b', label='follower')
            plt.plot(time, EDR_leader[time_range], 'r', label='leader')
            plt.legend()
            plt.xlabel("Time (UTC)")
            plt.ylabel("Eddy dissipation rate ($m^{2/3}/s$)")
            plt.title(Flight + '_' + sign + '_EDR')
            plt.xlim([time0, time1])
            ax = plt.axes()
            xticks = [time0 + m*60 for m in range(int(SI[i, 2])+1)]
            labels = [str(datetime.timedelta(seconds=(start_time + second)))[:-3] for second in xticks] # [:-3] for removing seconds
            plt.xticks(xticks, labels)
            plt.xticks(rotation=-90)
            plt.gcf().set_size_inches((11.5, 8.5), forward=False)
            os.chdir(Output_Directory + sign + '/')
            plt.savefig(Flight + '_' + sign + '_EDR' + '.tiff', bbox_inches="tight", pad_inches=0.06, dpi=300)
            #plt.show()

            plt.figure(Flight + '_' + sign + '_Lat')
            plt.plot(time, Lat[time_range], 'b', label='follower')
            plt.plot(time, Lat_leader[time_range], 'r', label='leader')
            plt.legend()
            plt.xlabel("Time (UTC)")
            plt.ylabel("A/C Latitude (angular degrees)")
            plt.title(Flight + '_' + sign + '_Lat')
            plt.xlim([time0, time1])
            ax = plt.axes()
            xticks = [time0 + m*60 for m in range(int(SI[i, 2])+1)]
            labels = [str(datetime.timedelta(seconds=(start_time + second)))[:-3] for second in xticks] # [:-3] for removing seconds
            plt.xticks(xticks, labels)
            plt.xticks(rotation=-90)
            plt.gcf().set_size_inches((11.5, 8.5), forward=False)
            os.chdir(Output_Directory + sign + '/')
            plt.savefig(Flight + '_' + sign + '_Lat' + '.tiff', bbox_inches="tight", pad_inches=0.06, dpi=300)
            #plt.show()

            plt.figure(Flight + '_' + sign + '_Lon')
            plt.plot(time, Lon[time_range], 'b', label='follower')
            plt.plot(time, Lon_leader[time_range], 'r', label='leader')
            plt.legend()
            plt.xlabel("Time (UTC)")
            plt.ylabel("A/C Longitude (angular degrees)")
            plt.title(Flight + '_' + sign + '_Lon')
            plt.xlim([time0, time1])
            ax = plt.axes()
            xticks = [time0 + m*60 for m in range(int(SI[i, 2])+1)]
            labels = [str(datetime.timedelta(seconds=(start_time + second)))[:-3] for second in xticks] # [:-3] for removing seconds
            plt.xticks(xticks, labels)
            plt.xticks(rotation=-90)
            plt.gcf().set_size_inches((11.5, 8.5), forward=False)
            os.chdir(Output_Directory + sign + '/')
            plt.savefig(Flight + '_' + sign + '_Lon' + '.tiff', bbox_inches="tight", pad_inches=0.06, dpi=300)
            #plt.show()

            plt.figure(Flight + '_' + sign + '_Alt')
            plt.plot(time, Alt[time_range], 'b', label='follower')
            plt.plot(time, Alt_leader[time_range], 'r', label='leader')
            plt.legend()
            plt.xlabel("Time (UTC)")
            plt.ylabel("A/C GPS Altitude (ft)")
            plt.title(Flight + '_' + sign + '_Alt')
            plt.xlim([time0, time1])
            ax = plt.axes()
            xticks = [time0 + m*60 for m in range(int(SI[i, 2])+1)]
            labels = [str(datetime.timedelta(seconds=(start_time + second)))[:-3] for second in xticks] # [:-3] for removing seconds
            plt.xticks(xticks, labels)
            plt.xticks(rotation=-90)
            plt.gcf().set_size_inches((11.5, 8.5), forward=False)
            os.chdir(Output_Directory + sign + '/')
            plt.savefig(Flight + '_' + sign + '_Alt' + '.tiff', bbox_inches="tight", pad_inches=0.06, dpi=300)
            #plt.show()
            ###############################################################################
            ###############################################################################
            # When follower becomes leader
            os.chdir('/Users/j6hickey/StratodynamicProject/Codes/FilteredSignal/' + Flight + '/')
            reversetime = open(Flight + '_ReverseTime.dat','w')
            if Flight == 'inbound':
                reverse = np.where(Lon_leader-Lon<0)[0] # all moments when reverse occurs (256 times per second)
            elif Flight == 'outbound':
                reverse = np.where(Lon_leader-Lon>0)[0] # all moments when reverse occurs (256 times per second)
            reverse = np.unique(np.floor(reverse/Fs)) # all the unique seconds when reverse occurs
            for i in reverse:
                reversetime.write(str(datetime.timedelta(seconds=(start_time + i))) + '\n')
            reversetime.close()
            ###############################################################################
            ###############################################################################
            # Correlation
            os.chdir(Output_Directory + sign + '/front/')
            corr = open(Flight + '_' + sign + '_front' + '_correlation_' + ND + '.dat','w')
            corr.write('Simple Correlation\n')
            corr.write('Vortex distance vs SPL\n')
            corr.write('%.4f'%Correlation(vor_dis, SPL_front, 0) + '\n')
            corr.write('Vortex distance vs Spectrum\n')
            corr.write('%.4f'%Correlation(vor_dis, spectrum_front, 0) + '\n')
            corr.write('SPL vs Spectrum\n')
            corr.write('%.4f'%Correlation(SPL_front, spectrum_front, 0) + '\n')
            corr.write('SPL vs Vortex age\n')
            corr.write('%.4f'%Correlation(SPL_front, VortexAge, 0) + '\n')
            corr.write('\n\n')
            corr.write('Pearson Correlation\n')
            corr.write('Vortex distance vs SPL\n')
            corr.write('%.4f'%PearsonCorrelation(vor_dis, SPL_front, 0) + '\n')
            corr.write('Vortex distance vs Spectrum\n')
            corr.write('%.4f'%PearsonCorrelation(vor_dis, spectrum_front, 0) + '\n')
            corr.write('SPL vs Spectrum\n')
            corr.write('%.4f'%PearsonCorrelation(SPL_front, spectrum_front, 0) + '\n')
            corr.write('SPL vs Vortex age\n')
            corr.write('%.4f'%PearsonCorrelation(SPL_front, VortexAge, 0) + '\n')
            corr.close()

            os.chdir(Output_Directory + sign + '/aft/')
            corr = open(Flight + '_' + sign + '_aft' + '_correlation_' + ND + '.dat','w')
            corr.write('Simple Correlation\n')
            corr.write('Vortex distance vs SPL\n')
            corr.write('%.4f'%Correlation(vor_dis, SPL_aft, 0) + '\n')
            corr.write('Vortex distance vs Spectrum\n')
            corr.write('%.4f'%Correlation(vor_dis, spectrum_aft, 0) + '\n')
            corr.write('SPL vs Spectrum\n')
            corr.write('%.4f'%Correlation(SPL_aft, spectrum_aft, 0) + '\n')
            corr.write('SPL vs Vortex age\n')
            corr.write('%.4f'%Correlation(SPL_aft, VortexAge, 0) + '\n')
            corr.write('\n\n')
            corr.write('Pearson Correlation\n')
            corr.write('Vortex distance vs SPL\n')
            corr.write('%.4f'%PearsonCorrelation(vor_dis, SPL_aft, 0) + '\n')
            corr.write('Vortex distance vs Spectrum\n')
            corr.write('%.4f'%PearsonCorrelation(vor_dis, spectrum_aft, 0) + '\n')
            corr.write('SPL vs Spectrum\n')
            corr.write('%.4f'%PearsonCorrelation(SPL_aft, spectrum_aft, 0) + '\n')
            corr.write('SPL vs Vortex age\n')
            corr.write('%.4f'%PearsonCorrelation(SPL_aft, VortexAge, 0) + '\n')
            corr.close()

            plt.close('all')

print(min(ps_front_min))
print(max(ps_front_max))
print(min(ps_aft_min))
print(max(ps_aft_max))
