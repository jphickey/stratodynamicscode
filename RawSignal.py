import scipy.io
import os
import numpy as np
from numpy.fft import fft, fftfreq, ifft
import matplotlib.pyplot as plt
from math import pi
import datetime
import matplotlib as mpl
from scipy.signal import butter, lfilter
import sys
import warnings
import pickle
from scipy.ndimage.filters import gaussian_filter
import pdb
from scipy.signal import find_peaks


if not sys.warnoptions:
    warnings.simplefilter("ignore")


def sharp_filter(data, lowcut, highcut, Fs):
    data_length = len(data) # number of samples
    freqs = fftfreq(data_length)*Fs # in Hz
    #freqs = (Fs/data_length)*np.arange(1,int(data_length/2)) # calculate manually
    freq_range = np.zeros_like(freqs, dtype=bool)
    for counter, i in enumerate(freqs):
        if i >= lowcut and i <= highcut:
            freq_range[counter] = 1
    spectrum_raw = fft(data)
    spectrum_filtered = spectrum_raw*freq_range
    return ifft(spectrum_filtered)

def butter_bandpass(lowcut, highcut, fs, order=5):
    nyq = 0.5 * fs
    low = lowcut / nyq
    high = highcut / nyq
    b, a = butter(order, [low, high], btype='band')
    return b, a

def butter_bandpass_filter(data, lowcut, highcut, fs, order=5):
    b, a = butter_bandpass(lowcut, highcut, fs, order=order)
    y = lfilter(b, a, data)
    return y

def normal_dist(x , mean , sd, SetMax1):
    prob_density = 1./(sd*np.sqrt(2*np.pi)) * np.exp(-0.5*(((x-mean)/sd)**2))
    if SetMax1 == True:
        prob_density = (prob_density-np.min(prob_density))/(np.max(prob_density)-min(prob_density))
    return prob_density

def LogTicks(a, b):
    ticks = []
    for i in range(int(np.floor(np.log10(a))), int(np.floor(np.log10(b)+1))):
        for j in range(1,10):
            if (10**i)*j >= a and (10**i)*j <=b:
                ticks = ticks + [(10**i)*j]
    return np.log10(ticks)

def WindowAvg (Signal, WindowSize):
    Result = np.convolve(Signal, np.ones((WindowSize,))/WindowSize, mode='valid')
    LeftValue = np.average(Signal[:WindowSize])
    RightValue = np.average(Signal[Signal.size-WindowSize:])
    LeftArray = LeftValue * np.ones((int(WindowSize/2)))
    RightArray = RightValue * np.ones((int((WindowSize-1)/2)))
    Result = np.append(LeftArray, Result)
    Result = np.append(Result, RightArray)
    return Result

def Correlation (A, B, n):
    if n == 0:
        return np.dot(A,B)/(np.linalg.norm(A)*np.linalg.norm(B))
    elif n > 0:
        return np.dot(A[:-n],B[n:])/(np.linalg.norm(A[:-n])*np.linalg.norm(B[n:]))
    elif n < 0:
        n = -n
    return np.dot(A[n:],B[:-n])/(np.linalg.norm(A[n:])*np.linalg.norm(B[:-n]))

def PearsonCorrelation (AA, BB, n):
    # Eq.3 of https://en.wikipedia.org/wiki/Pearson_correlation_coefficient
    A = AA - np.mean(AA)
    B = BB - np.mean(BB)
    if n == 0:
        return np.dot(A,B)/(np.linalg.norm(A)*np.linalg.norm(B))
    elif n > 0:
        return np.dot(A[:-n],B[n:])/(np.linalg.norm(A[:-n])*np.linalg.norm(B[n:]))
    elif n < 0:
        n = -n
    return np.dot(A[n:],B[:-n])/(np.linalg.norm(A[n:])*np.linalg.norm(B[:-n]))

def savitzky_golay(y, window_size, order, deriv=0, rate=1):
    r"""Smooth (and optionally differentiate) data with a Savitzky-Golay filter.
    The Savitzky-Golay filter removes high frequency noise from data.
    It has the advantage of preserving the original shape and
    features of the signal better than other types of filtering
    approaches, such as moving averages techniques.
    Parameters
    ----------
    y : array_like, shape (N,)
        the values of the time history of the signal.
    window_size : int
        the length of the window. Must be an odd integer number.
    order : int
        the order of the polynomial used in the filtering.
        Must be less then `window_size` - 1.
    deriv: int
        the order of the derivative to compute (default = 0 means only smoothing)
    Returns
    -------
    ys : ndarray, shape (N)
        the smoothed signal (or it's n-th derivative).
    Notes
    -----
    The Savitzky-Golay is a type of low-pass filter, particularly
    suited for smoothing noisy data. The main idea behind this
    approach is to make for each point a least-square fit with a
    polynomial of high order over a odd-sized window centered at
    the point.
    Examples
    --------
    t = np.linspace(-4, 4, 500)
    y = np.exp( -t**2 ) + np.random.normal(0, 0.05, t.shape)
    ysg = savitzky_golay(y, window_size=31, order=4)
    import matplotlib.pyplot as plt
    plt.plot(t, y, label='Noisy signal')
    plt.plot(t, np.exp(-t**2), 'k', lw=1.5, label='Original signal')
    plt.plot(t, ysg, 'r', label='Filtered signal')
    plt.legend()
    plt.show()
    References
    ----------
    .. [1] A. Savitzky, M. J. E. Golay, Smoothing and Differentiation of
       Data by Simplified Least Squares Procedures. Analytical
       Chemistry, 1964, 36 (8), pp 1627-1639.
    .. [2] Numerical Recipes 3rd Edition: The Art of Scientific Computing
       W.H. Press, S.A. Teukolsky, W.T. Vetterling, B.P. Flannery
       Cambridge University Press ISBN-13: 9780521880688
    """
    import numpy as np
    from math import factorial

    try:
        window_size = np.abs(np.int(window_size))
        order = np.abs(np.int(order))
    except ValueError:
        raise ValueError("window_size and order have to be of type int")
    if window_size % 2 != 1 or window_size < 1:
        raise TypeError("window_size size must be a positive odd number")
    if window_size < order + 2:
        raise TypeError("window_size is too small for the polynomials order")
    order_range = range(order+1)
    half_window = (window_size -1) // 2
    # precompute coefficients
    b = np.mat([[k**i for i in order_range] for k in range(-half_window, half_window+1)])
    m = np.linalg.pinv(b).A[deriv] * rate**deriv * factorial(deriv)
    # pad the signal at the extremes with
    # values taken from the signal itself
    firstvals = y[0] - np.abs( y[1:half_window+1][::-1] - y[0] )
    lastvals = y[-1] + np.abs(y[-half_window-1:-1][::-1] - y[-1])
    y = np.concatenate((firstvals, y, lastvals))
    return np.convolve( m[::-1], y, mode='valid')
###############################################################################
###############################################################################
# Initial setup

ps_front_min = []
ps_front_max = []
ps_aft_min = []
ps_aft_max = []

#Flight = 'inbound'
#sign = 'A'
#ND = 'off' # Normal_Distribution_Factor
myFlights= ['inbound', 'outbound']      #['inbound', 'outbound']
mySign   = ['A', 'B', 'C', 'D']            #['A', 'B', 'C', 'D']
myND     = ['off']                #['off', 'on']

for Flight in myFlights:
    for iii,sign in enumerate(mySign):
        for ND in myND:

            print('processing ' + Flight + '_' + sign + '_' + ND)

            if Flight == 'inbound':
                # Start of the flight: 16:53:54.000
                start_time = 16*3600 + 53*60 + 54 # in seconds

                # Signature Sign
                SS = {'A':0, 'B':1, 'C':2, 'D':3}

                SI = np.zeros((4, 3)) # SI[i,0]: start hour of i-th signature
                                                                                        # SI[i,1]: start minute of i-th signature
                                                                                        # SI[i,2]: time interval of i-th signature in minute

                SI[0, 0] = 18
                SI[0, 1] = 0
                SI[0, 2] = 15

                SI[1, 0] = 19
                SI[1, 1] = 0
                SI[1, 2] = 30

                SI[2, 0] = 20
                SI[2, 1] = 25
                SI[2, 2] = 15

                SI[3, 0] = 21
                SI[3, 1] = 30
                SI[3, 2] = 30

            elif Flight == 'outbound':
                # Start of the flight: 08:14:00.000
                start_time = 8*3600 + 14*60 + 0 # in seconds

                # Signature Sign
                SS = {'A':0, 'B':1, 'C':2, 'D':3}

                SI = np.zeros((4, 3)) # SI[i,0]: start hour of i-th signature
                                                                                        # SI[i,1]: start minute of i-th signature
                                                                                        # SI[i,2]: time interval of i-th signature in minute

                SI[0, 0] = 8
                SI[0, 1] = 50
                SI[0, 2] = 30

                SI[1, 0] = 10
                SI[1, 1] = 5
                SI[1, 2] = 30

                SI[2, 0] = 13
                SI[2, 1] = 15
                SI[2, 2] = 15

                SI[3, 0] = 14
                SI[3, 1] = 0
                SI[3, 2] = 15
            ###############################################################################
            ###############################################################################
                # Load data
            Input_Directory = '/Users/j6hickey/StratodynamicProject/outData/'
            Output_Directory = '/Users/j6hickey/StratodynamicProject/Codes_new/FilteredSignal/' + Flight + '/' + ND +'/'

            os.chdir(Input_Directory)
            time = np.load(Flight + '_follower_GMTs.npy')[:, 0]
            VortexAge = np.load(Flight + '_follower_VortexAge.npy')[:, 0]
            LatDis = np.load(Flight + '_follower_LatDis.npy')[:, 0]
            VerDis = np.load(Flight + '_follower_VerDis.npy')[:, 0]
            signal_front_raw = np.load(Flight + '_follower_front.npy')[:, 0]
            signal_aft_raw = np.load(Flight + '_follower_aft.npy')[:, 0]
            VLF = np.load(Flight + '_follower_VLF.npy')[:, 0]
            EDR = np.load(Flight + '_follower_EDR.npy')[:, 0]
            Lat = np.load(Flight + '_follower_Lat.npy')[:, 0]
            Lon = np.load(Flight + '_follower_Lon.npy')[:, 0]
            Alt = np.load(Flight + '_follower_Alt.npy')[:, 0]
            Sep = np.load(Flight + '_follower_Sep.npy')[:, 0]
            VLF_leader = np.load(Flight + '_leader_VLF.npy')[:, 0]
            EDR_leader = np.load(Flight + '_leader_EDR.npy')[:, 0]
            Lat_leader = np.load(Flight + '_leader_Lat.npy')[:, 0]
            Lon_leader = np.load(Flight + '_leader_Lon.npy')[:, 0]
            Alt_leader = np.load(Flight + '_leader_Alt.npy')[:, 0]
            ###############################################################################
            ###############################################################################
            # SPL
            Fs = 256 # Frequency of sampling, given in the readme file

            # time variable in this code is considered from the start of the flight
            time = time - time[0]

            # Infrasonic signals (measured in volts) are converted to milli-volts
            signal_front_raw = signal_front_raw * 1000
            signal_aft_raw = signal_aft_raw * 1000

            # Sensitivity of the microphones
            # The sensitivity of 1.5in microphone (installed in the front of the cabin) is 118mV/Pa
            Sensitivity_front = 118
            # The sensitivity of 3in microphone (installed in the aft of the cabin) is 338mV/Pa
            Sensitivity_aft = 338

            # Corresponding pressure perturbation in Pa
            signal_front_raw = signal_front_raw / Sensitivity_front
            signal_aft_raw = signal_aft_raw / Sensitivity_aft

            # Sensitivity of the microphones to low frequencies
            # The sensitivity of 1.5in microphone (installed in the front of the cabin) is 0.1 Hz
            lowcut_front = 0.1
            # The sensitivity of 3in microphone (installed in the aft of the cabin) is 0.001 Hz
            lowcut_aft = 0.001

            # Sharp filtering to keep infrasonic frequencies
            highcut = 20
            signal_front = sharp_filter(signal_front_raw, lowcut_front, highcut, Fs)
            signal_aft = sharp_filter(signal_aft_raw, lowcut_aft, highcut, Fs)

            ###############################################################################
            ###############################################################################
            # Pressure mean value
            os.chdir('/Users/j6hickey/StratodynamicProject/Codes_new/FilteredSignal/' + Flight + '/')

            ###############################################################################
            ###############################################################################
            # Plot power spectra (power vs frequency)
            i = SS[sign]

            time0 = SI[i, 0]*3600 + SI[i, 1]*60 - start_time # seconds passed from the start_time
            time1 = time0 + SI[i, 2]*60 # seconds passed from the start_time

            index0 = int(time0 * Fs)
            index1 = int(time1 * Fs)

            #Vertical Load factor
            plt.figure(1)
            maxVLF=np.max(VLF)
            if iii==0:
              plt.plot(time,VLF)
              xticks = [ m*60*60 for m in range(int(7)+1)]
              labels = [str(datetime.timedelta(seconds=(start_time + second)))[:-3] for second in xticks] # [:-3] for removing seconds
              pdb.set_trace()
              plt.xticks(xticks, labels)
              plt.xticks(rotation=-90)
            plt.axvspan(time0, time1, facecolor='b', alpha=0.25)
            plt.text(time0,maxVLF,sign,fontsize='large')
            plt.xlabel("Time (UTC)")
            plt.ylabel("VLF")

            # Distance between planes
            plt.figure(2)
            dist=(LatDis**2+VerDis**2)**0.5
            mask = dist < 750
            maxDist=740
            if iii==0:
              plt.plot(time[mask],dist[mask])
              plt.xticks(xticks, labels)
              plt.xticks(rotation=-90)
            plt.axvspan(time0, time1, facecolor='b', alpha=0.25)
            plt.text(time0,maxDist,sign,fontsize='large')
            plt.xlabel("Time (UTC)")
            plt.ylabel("distance (m)")

            # EDR
            plt.figure(3)
            mask = EDR < 750
            maxEDR=max(EDR)
            if iii==0:
              plt.plot(time[mask],EDR[mask])
              plt.xticks(xticks, labels)
              plt.xticks(rotation=-90)
            plt.axvspan(time0, time1, facecolor='b', alpha=0.25)
            plt.text(time0,maxEDR,sign,fontsize='large')
            plt.xlabel("Time (UTC)")
            plt.ylabel("Eddy dissipation rate (EDR)")

            #signal_aft
            plt.figure(4)
            mask = signal_aft < 750
            maxEDR=max(signal_aft)
            if iii==0:
              plt.plot(time[mask],signal_aft[mask])
              plt.xticks(xticks, labels)
              plt.xticks(rotation=-90)
              print(labels)
            plt.axvspan(time0, time1, facecolor='b', alpha=0.25)
            plt.text(time0,maxEDR,sign,fontsize='large')
            plt.xlabel("Time (UTC)")
            plt.ylabel("Aft microphone signal")

    plt.figure(1)
    plt.tight_layout()
    plt.savefig(Flight + '_VLF' + '.tiff', bbox_inches="tight", pad_inches=0.06, dpi=300)

    plt.figure(2)
    plt.tight_layout()
    plt.savefig(Flight + '_distance' + '.tiff', bbox_inches="tight", pad_inches=0.06, dpi=300)

    plt.figure(3)
    plt.tight_layout()
    plt.savefig(Flight + '_EDR' + '.tiff', bbox_inches="tight", pad_inches=0.06, dpi=300)

    plt.figure(4)
    plt.tight_layout()
    plt.savefig(Flight + '_aftMic' + '.tiff', bbox_inches="tight", pad_inches=0.06, dpi=300)
    print(Flight + '_EDR' + '.tiff')
    plt.close('all')
plt.show()
